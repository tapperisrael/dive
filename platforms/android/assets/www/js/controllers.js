angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$state,$localStorage,$rootScope,$ionicPopup,$translate) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  $scope.$on('$ionicView.enter', function(e) {
  

  	$scope.insuranceError = $translate.instant('insurance_error');
  	$scope.confirmError = $translate.instant('confirm_error');

	
	$scope.checkClubOwner = function()
	{
		if ($localStorage.club_id > 0)
			$scope.isClubOwner = true
		else
			$scope.isClubOwner = false;
	}
	
	//$scope.checkClubOwner();
	
	$scope.setLanguage = function()
	{
		if ($localStorage.defaultLanguage)
			$translate.use($localStorage.defaultLanguage);
	}
	
	$scope.setLanguage();
	

  $scope.LogOut = function()
  {
	  $localStorage.userid = '';
	  $localStorage.name = '';
	  $localStorage.email = '';
	  $localStorage.phone = '';
	  $localStorage.birthdate = '';
	  $localStorage.country = '';
	  $rootScope.DivesCount = 1;
	  $rootScope.sendClub = '';
	  $localStorage.gender = '';
	  $localStorage.personalid = '';
	  $localStorage.club_id = '';
	  $state.go('app.login');		
  }
  
  $scope.name = $localStorage.name;
  

  $scope.NavigateUrl = function(Url)
  {
	  if (Url == 'insurance')
	  {
			$ionicPopup.alert({
			 title: $scope.insuranceError,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });
	  }
	  else
	  {
		  $state.go('app.'+Url);
	  }
	


	 //$state.go(Url);
	//window.location = "#/app/"+Url;
  }
  

 	$scope.goBack = function()
	{
		window.history.back();
		
	} 
  
  
});  
 
})



.controller('LoginCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$q,$localStorage,$ionicSideMenuDelegate,$ionicModal,$ionicLoading,$translate) {
	
	if ($localStorage.userid)
	{
		$state.go('app.main');
	}
	
	$scope.sendtoserver = true;

	$rootScope.currentPage = 'login';
	$ionicSideMenuDelegate.canDragContent(false);
	
	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("מסך התחברות");
		}
	});


	$scope.changeLanguage = function (langKey) {
		$translate.use(langKey);
		$rootScope.DefaultLanguage = langKey;
		$localStorage.defaultLanguage = langKey;
		$scope.getLanguagesImages();
		
	};	


	$rootScope.$watch("LanguagesJson", function(){
       
	   $scope.LoginImage = $rootScope.LanguagesJson[0][$rootScope.DefaultLanguage];
	   $scope.RegisterImage = $rootScope.LanguagesJson[1][$rootScope.DefaultLanguage];
	   $scope.ForgotImage = $rootScope.LanguagesJson[2][$rootScope.DefaultLanguage];		
	   
	   //console.log ("login watch " , $scope.LoginBtn);
	   $scope.getLanguageText();
	   //[$rootScope.DefaultLanguage];

    });

	$scope.getLanguagesImages = function()
	{
	   $scope.LoginImage = $rootScope.LanguagesJson[0][$rootScope.DefaultLanguage];
	   $scope.RegisterImage = $rootScope.LanguagesJson[1][$rootScope.DefaultLanguage];		
	   $scope.ForgotImage = $rootScope.LanguagesJson[2][$rootScope.DefaultLanguage];		
	   
	   
	   //$scope.getLanguageText();	
	}

  $rootScope.$on('$translateChangeSuccess', function () {
    $scope.getLanguageText();
  });

  
	$scope.getLanguageText = function()
	{
		$scope.loginError1 = $translate.instant('loginError1');
		$scope.loginError2 = $translate.instant('loginError2');
		$scope.loginError3 = $translate.instant('loginError3');
		$scope.loginError4 = $translate.instant('loginError4');
		$scope.loginError5 = $translate.instant('loginError5');
		$scope.loginError6 = $translate.instant('loginError6');
		$scope.loginError7 = $translate.instant('loginError7');
		$scope.loginError8 = $translate.instant('loginError8');	
		$scope.confirmError = $translate.instant('confirm_error');
		
	}

	$scope.getLanguageText();
	

	$scope.login = 
	{
		"username" : "",
		"password" : ""
	}
	
	$scope.forgot = 
	{
		"email" : ""
	}
	
	$scope.LoginBtn = function()
	{
	  if ($scope.sendtoserver)
	  {
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		$scope.sendtoserver = false;
	
		if ($scope.login.username == "")
		{
			$ionicPopup.alert({
			 title: $scope.loginError1,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });
		   $scope.sendtoserver = true;
		}
		else if ($scope.login.password == "")
		{
			$ionicPopup.alert({
			 title: $scope.loginError2,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });
		   $scope.sendtoserver = true;
		}
		else 
		{
			login_data = 
			{
				"username" : $scope.login.username,
				"password" : $scope.login.password,
				"push_id" : $rootScope.pushId
			}					
			$http.post($rootScope.Host+'/login.php',login_data).success(function(data)
			{
				$scope.sendtoserver = true;
				
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					 title: $scope.loginError3,
					buttons: [{
						text: $scope.confirmError,
						type: 'button-positive',
					  }]
				   });					
				}
				else
				{
					$localStorage.userid = data.response.userid;
					$localStorage.name = data.response.name;
					$localStorage.email = data.response.email;
					$localStorage.phone = data.response.phone;
					$localStorage.birthdate = data.response.birthdate;
					$localStorage.country = data.response.country;
					$localStorage.gender = data.response.gender;
					$localStorage.personalid = data.response.personalid;
					$localStorage.club_id = data.response.club_id;
					$localStorage.push_enabled = data.response.push_enabled;
					$scope.checkClubOwner();
					$state.go('app.main');
				}
			});
		}		  
	  }

	}
	
	$scope.RegisterBtn = function()
	{
		$state.go('app.register');
	}

	$scope.forgotpassBtn = function()
	{

		$ionicModal.fromTemplateUrl('templates/forgot_password.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	
	}

	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}
	
	
	$scope.sendPassword = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		var emailregex = /\S+@\S+\.\S+/;
		

		if ($scope.forgot.email =="")
		{
			$ionicPopup.alert({
			 title: $scope.loginError4,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });				
		}
		
		else if (!emailregex.test($scope.forgot.email))
		{
			$ionicPopup.alert({
			 title: $scope.loginError5,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });				
		}		
	

		else
		{
			send_data = 
			{
				"email" : $scope.forgot.email,
				"send" : 1
			
			}					
			$http.post($rootScope.Host+'/forgot_pass.php',send_data).success(function(data)
			{

			});
			
			$ionicPopup.alert({
			 title: $scope.loginError6,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });	

		   $scope.forgot.email = '';
		   $scope.modal.hide();
		}		
	}
	

	
  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {


	
	$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email,profileInfo.gender);

	
      $ionicLoading.hide();
      //$state.go('app.home');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name,gender,first_name,last_name,locale&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.FaceBookLoginBtn = function() {
    facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);


					getFacebookProfileInfo(success.authResponse)
					.then(function(profileInfo) {

							
						$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email,profileInfo.gender);
							


						//$state.go('app.home');
					}, function(fail){
						// Fail get profile info
						console.log('profile info fail', fail);
					});
				
      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
				// but has not authenticated your app
        // Else the person is not logged into Facebook,
				// so we're not sure if they are logged into this app or not.

				//console.log('getLoginStatus', success.status);
 
				$ionicLoading.show({
				  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
				});
 

				// Ask the permissions you need. You can learn more about
				// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };

	$scope.showOptions = function() 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: $scope.loginError7,
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: $scope.loginError8,
		type: 'button-positive',
		onTap: function(e) { 
		  $state.go('app.updateinfo');		
		}
	   },
	   	   {
		text: $scope.loginError9,
		type: 'button-assertive',
		onTap: function(e) {  
		  $state.go('app.main');		
		}
	   },
	   ]
	  });
	  

	};	  
  
  $scope.FacebookLoginFunction = function(id,firstname,lastname,email,gender)
  {
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		$scope.fullname = firstname+' '+lastname;
		$scope.gender = (gender == "male" ? "זכר" : "נקבה");
			
			facebook_data = 
			{
				"id" : id,
				"firstname" : firstname,
				"lastname" : lastname,
				"email" : email,
				"fullname" : $scope.fullname,
				"gender" : $scope.gender
			}					
			$http.post($rootScope.Host+'/facebook_connect.php',facebook_data).success(function(data)
			{
			//	if (data.response.userid)
			//	{
					$localStorage.userid = data.response.userid;
					$localStorage.name = $scope.fullname;
					$localStorage.email = email;
					$localStorage.gender = $scope.gender;
					$localStorage.birthdate = data.response.birthdate;
					$localStorage.phone = data.response.phone;
					$localStorage.country = data.response.country;
					$localStorage.club_id = data.response.club_id;
					
					$scope.checkClubOwner();
					
					
					if ($localStorage.birthdate == "" || $localStorage.birthdate == undefined)
					{
						$scope.showOptions();
					}
					else if ($localStorage.phone == "" ||  $localStorage.phone == undefined)
					{
						$scope.showOptions();
					}

					else
					{
						$state.go('app.main');	
					}
				
				//}
				/*
				else
				{
					$ionicPopup.alert({
					 title: 'שגיאה בהתחברות יש לנסות שוב',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });						
				}
				*/	
			});
  }
	
	
	
})

.controller('RegisterCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicLoading,$translate,$ionicModal) {

  /*
    $ionicLoading.show({
      template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
    });
  */
  
	
	if ($localStorage.userid)
		$state.go('app.main');
	
	$rootScope.currentPage = 'register';
	$scope.AboutArray = $rootScope.AboutTextArray;
	$ionicSideMenuDelegate.canDragContent(false);
	
	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("מסך הרשמה");
		}
	});

	$scope.getLanguagesImages = function()
	{
	   $scope.LoginImage = $rootScope.LanguagesJson[0][$rootScope.DefaultLanguage];
	   $scope.RegisterImage = $rootScope.LanguagesJson[1][$rootScope.DefaultLanguage];	
	   
	}

	$scope.getLanguagesImages();
	
	$scope.registerError1 = $translate.instant('registerError1');
	$scope.registerError2 = $translate.instant('registerError2');
	$scope.registerError3 = $translate.instant('registerError3');
	$scope.registerError4 = $translate.instant('registerError4');
	$scope.registerError5 = $translate.instant('registerError5');
	$scope.registerError6 = $translate.instant('registerError6');
	$scope.registerError7 = $translate.instant('registerError7');
	$scope.registerError8 = $translate.instant('registerError8');
	$scope.registerError9 = $translate.instant('registerError9');
	$scope.registerError10 = $translate.instant('registerError10');
	$scope.registerError11 = $translate.instant('registerError11');
	
	$scope.registerAgree = $translate.instant('registerAgree');
	
	$scope.confirmError = $translate.instant('confirm_error');
	
	if ($rootScope.DefaultLanguage =="he")
	{
		$scope.languagesJson = $rootScope.AllHebrewCountries;
		//$scope.languagesJson = $rootScope.HebrewCountries;
	}
	else
	{
		$scope.languagesJson = $rootScope.AllEnglishCountries;
		//$scope.languagesJson = $rootScope.EnglishCountries;
	}
	
	$scope.register = 
	{
		"name": "",
		"first": "",
		"last": "",		
		"country": "",
		"email": "",
		"birth": "",
		"personalid" : "",
		"gender": "",
		"phone": "",
		"username": "",		
		"password": "",
		"agree" : false
	}
	
	
	$scope.openTerms = function()
	{
		if ($rootScope.DefaultLanguage == "he")
			$scope.termsPage = "templates/terms_he.html";
			else
			$scope.termsPage = "templates/terms_en.html";
		
		
		$ionicModal.fromTemplateUrl($scope.termsPage, {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(termsModal) {
		  $scope.termsModal = termsModal;
		  $scope.termsModal.show();
		 });		
	}
	
	$scope.closeTerms = function()
	{
		$scope.termsModal.hide();
	}
	
	
	
	$scope.RegisterBtn = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		var emailregex = /\S+@\S+\.\S+/;
		
		if ($scope.register.first =="")
		{
			$ionicPopup.alert({
			 title: $scope.registerError1,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.register.last =="")
		{
			$ionicPopup.alert({
			 title: $scope.registerError2,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });				
		}
		
		else if ($scope.register.phone =="")
		{
			$ionicPopup.alert({
			 title: $scope.registerError3,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });				
		}
		
		else if ($scope.register.email =="")
		{
			$ionicPopup.alert({
			 title: $scope.registerError4,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });				
		}
		
		else if (!emailregex.test($scope.register.email))
		{
			$ionicPopup.alert({
			 title: $scope.registerError5,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });			
				
			$scope.register.email = '';
		   
		}		
		// else if ($scope.register.birth =="")
		// {
		// 	$ionicPopup.alert({
		// 	 title: $scope.registerError6,
		// 	buttons: [{
		// 		text: $scope.confirmError,
		// 		type: 'button-positive',
		// 	  }]
		//    });
		// }
		// else if ($scope.register.personalid =="")
		// {
		// 	$ionicPopup.alert({
		// 	 title: $scope.registerError11,
		// 	buttons: [{
		// 		text: $scope.confirmError,
		// 		type: 'button-positive',
		// 	  }]
		//    });
		// }
		else if ($scope.register.username =="")
		{
			$ionicPopup.alert({
			 title: $scope.registerError7,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });				
		}
		else if ($scope.register.password =="")
		{
			$ionicPopup.alert({
			 title: $scope.registerError8,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });	
		}
		else if ($scope.register.agree == false)
		{
			$ionicPopup.alert({
			 title: $scope.registerAgree,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });	
		}		
		else
		{
			
			$scope.fullname = $scope.register.first+' '+$scope.register.last;
			
			register_data = 
			{
				"name" : $scope.fullname,
				"email" : $scope.register.email,
				"birth" : $scope.register.birth,
				"personalid" : $scope.register.personalid,
				"phone" : $scope.register.phone,
				"country" : $scope.register.country,
				"gender" : $scope.register.gender,
				"username" : $scope.register.username,
				"password" : $scope.register.password,
				"push_id" : $rootScope.pushId				
			}					
			$http.post($rootScope.Host+'/register.php',register_data).success(function(data)
			{
				
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					 title: $scope.registerError9,
					buttons: [{
						text: $scope.confirmError,
						type: 'button-positive',
					  }]
				   });		
				   
					//$scope.register.personalid = '';
					$scope.register.username = '';
					$scope.register.email = '';
				}
				else
				{
					$localStorage.userid = data.response.userid;
					$localStorage.name = $scope.fullname;
					$localStorage.email = $scope.register.email;
					$localStorage.phone = $scope.register.phone;
					$localStorage.country = $scope.register.country;
					$localStorage.birthdate = $scope.register.birth;
					$localStorage.gender = $scope.register.gender;
					$localStorage.personalid = $scope.register.personalid;
					$localStorage.push_enabled = "0";
					$state.go('app.main');					
				}
				
				$scope.register.agree = false;


			});
		}
		
	}
	
	
	$scope.LoginBtn = function()
	{
		$state.go('app.login');
	}
	

	
})


.controller('MainCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicPlatform,$translate,$ionicModal,$timeout) {
	
	//console.log($localStorage);
	//$ionicSideMenuDelegate.canDragContent(false);
	$rootScope.currentPage = 'main';
	$rootScope.phphost = $rootScope.Host;
	
	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("מסך ראשי");
		}
		$scope.getMessagesCount();
	});
	
	$rootScope.$on('pushmessage', function(event, args) 
	{
		 $timeout(function() 
		 {
			$scope.getMessagesCount();
		 }, 300);
	});

	
	$scope.changeLanguage = function (langKey) {
		$translate.use(langKey);
		$rootScope.DefaultLanguage = langKey;
		$localStorage.defaultLanguage = langKey;
		$scope.getLanguagesImages();
	};
	
	$scope.ForgotImage = 'img/approved.png';
	
	
	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
		   $scope.MainImage1 = $rootScope.LanguagesJson[3][$rootScope.DefaultLanguage];
		   $scope.MainImage2 = $rootScope.LanguagesJson[4][$rootScope.DefaultLanguage];		
		   $scope.MainImage3 = $rootScope.LanguagesJson[5][$rootScope.DefaultLanguage];		
		   $scope.MainImage4 = $rootScope.LanguagesJson[6][$rootScope.DefaultLanguage];		
		   
	   }

    });
	
	$scope.getLanguagesImages = function()
	{
		   $scope.MainImage1 = $rootScope.LanguagesJson[3][$rootScope.DefaultLanguage];
		   $scope.MainImage2 = $rootScope.LanguagesJson[4][$rootScope.DefaultLanguage];		
		   $scope.MainImage3 = $rootScope.LanguagesJson[5][$rootScope.DefaultLanguage];		
		   $scope.MainImage4 = $rootScope.LanguagesJson[6][$rootScope.DefaultLanguage];			

	}
	
	
	   $scope.insuranceError = $translate.instant('insurance_error');
	   $scope.confirmError = $translate.instant('confirm_error');
	   $scope.insuranceText1 = $translate.instant('main_insuranceoptions1');
	   $scope.insuranceText2 = $translate.instant('main_insuranceoptions2');
	   $scope.insuranceText3 = $translate.instant('main_insuranceoptions3');
	   $scope.insuranceText4 = $translate.instant('main_insuranceoptions4');
	   $scope.insuranceText5 = $translate.instant('main_insuranceoptions5');
	   
	   
	$scope.getMessagesCount = function()
	{
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		send_data = 
		{
			"user" : $localStorage.userid
		}	

				
		$http.post($rootScope.Host+'/get_push_count.php',send_data).success(function(data)
		{
			$rootScope.TotalMessagesCount = data[0].count;
		});	
	}		
	   
	$scope.DivingLicenseBtn = function()
	{
		$state.go('app.divinglicense');
	}

	$scope.DivingDiaryBtn = function()
	{
		$state.go('app.divingdiary');
	}	

	$scope.maintenance  = function()
	{
		$ionicPopup.alert({
		 title: $scope.insuranceError,
		buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
		  }]
	   });
	}

	$scope.insuranceOptions = function() 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: $scope.insuranceText1,
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [
	   {
		text: $scope.insuranceText2,
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },

	   {
		text: $scope.insuranceText3,
		type: 'button-calm',
		onTap: function(e) { 
		
			iabRef = window.open('http://insurance.idive.co.il/Insurance/Buy?agentid=21621','_blank', 'location=yes');
		  
		}
	   },
	   {
		text: $scope.insuranceText4,
		type: 'button-calm',
		onTap: function(e) { 
		
			iabRef = window.open('http://insurance.idive.co.il/Insurance/Search','_blank', 'location=yes');
		 
		}
	   },	   
	   {
		text: $scope.insuranceText5,
		type: 'button-calm',
		onTap: function(e) { 
		 window.location = "#/app/insurance";
		}
	   }

	   ]
	  });
	  
	  

	};	
	
	$scope.selectWeather = function(index)
	{
		for (i = 0; i < $rootScope.WeatherArray.length; i++) 
		{
			$rootScope.WeatherArray[i].selected = "0";
		}
		
		$rootScope.WeatherArray[index].selected = "1";
		
		$rootScope.GetWeatherAPI($rootScope.WeatherArray[index].location_lat,$rootScope.WeatherArray[index].location_lng);
		$rootScope.SelectedWeatherCity = $rootScope.WeatherArray[index].title;
		$rootScope.SelectedWeatherCityEn = $rootScope.WeatherArray[index].title_english;
		$scope.closeWeatherModal();
	}
	

	$scope.openWeatherModal = function()
	{
		$ionicModal.fromTemplateUrl('templates/weather_modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(WeatherModal) {
		  $scope.WeatherModal = WeatherModal;
		  $scope.WeatherModal.show();
		 });
	}
	

	$scope.closeWeatherModal = function()
	{
		$scope.WeatherModal.hide();
	}		
	
})

.controller('DivingLicenseCtrl', function($scope, $stateParams,$ionicModal,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$ionicHistory) {



  $scope.$on('$ionicView.enter', function(e) {
	  
	if(window.cordova){
		window.ga.trackView("צילום תעודת צולל ראשי");
	}
		
	 // $scope.navTitle='<img class="title-image" src="img/logoIcon.png" />'
	  $scope.navTitle='<p class="title-image">צילום רישיון צלילה</p>'
	  $scope.ImgHost = $rootScope.Host;
	  $rootScope.currentPage = 'divinglicense';
	  $scope.LicenseImage = 'img/addphoto.png';
	  $scope.phpHost = $rootScope.Host;
 // $ionicNavBarDelegate.showBackButton(true);
 
	 $ionicHistory.nextViewOptions({
		disableBack: false
	  });

	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
			$scope.DivingImage1 = $rootScope.LanguagesJson[7][$rootScope.DefaultLanguage];	 
			$scope.DivingImage2 = $rootScope.LanguagesJson[31][$rootScope.DefaultLanguage];
           $scope.MainLicenseImage117 = $rootScope.LanguagesJson[27][$rootScope.DefaultLanguage];
	   }

    });




      $scope.getInsurance = function()
      {
          $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

          license_data =
              {
                  "user" : $localStorage.userid
              }
          $http.post($rootScope.Host+'/get_insurance.php',license_data).success(function(data)
          {
              console.log("ResponseImage : " , $rootScope.Host+data.response.image)
              if (data.response.image)
              {
                  $scope.uploadedimage12 = $rootScope.Host+data.response.image;
                  $scope.serverImage = data.response.image;
              }
              else
              {
                  $scope.uploadedimage = 'img/addphoto.png';
                  $scope.serverImage = "";
              }


          });
      }

      $scope.getInsurance();


      //$ionicSideMenuDelegate.canDragContent(false);
	
		$scope.getLicenses = function()
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				license_data = 
				{
					"user" : $localStorage.userid
				}					
				$http.post($rootScope.Host+'/get_diving_license.php',license_data).success(function(data)
				{
					$scope.licenses = data.response;
					//alert ($scope.licenses.length)
					
					//console.log($scope.licenses);
				});				
		}
		$scope.getMainLicense = function()
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				license_data = 
				{
					"user" : $localStorage.userid
				}					
				$http.post($rootScope.Host+'/get_main_license.php',license_data).success(function(data)
				{
					$rootScope.licenseJson = data.response;
					
					if (data.response[0].userlicense)
					{
						$scope.LicenseImage = $rootScope.Host+data.response[0].userlicense;
					}
					
					//console.log($scope.licenses);
				});				
		}	
		$scope.getLicenses();
		$scope.getMainLicense();



      $scope.ShowImageModal = function(index)
      {
          $scope.licenseImageModal = $scope.uploadedimage12;

          $ionicModal.fromTemplateUrl('image-modal.html', {
              scope: $scope,
              animation: 'slide-in-up'
          }).then(function(modal) {
              $scope.modal = modal;
              $scope.modal.show();
          });
      }

      $scope.closeModal = function()
      {
          $scope.modal.hide();
      }


});	
})

.controller('AddLicenseCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicActionSheet,$ionicSideMenuDelegate,$cordovaCamera,$timeout,$ionicModal,$translate) {
	
	//$ionicSideMenuDelegate.canDragContent(false);
	$scope.navTitle='<p class="title-image">הוסף רישיון צלילה</p>';
	$rootScope.currentPage = 'addlicense';
	
	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("הוספת תעודת צולל");
		}
	});	

	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
		 $scope.MainLicenseImage1 = $rootScope.LanguagesJson[8][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage2 = $rootScope.LanguagesJson[9][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage3 = $rootScope.LanguagesJson[10][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage4 = $rootScope.LanguagesJson[11][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage5 = $rootScope.LanguagesJson[12][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage6 = $rootScope.LanguagesJson[13][$rootScope.DefaultLanguage];   
	   }

    });


	$scope.getLicenseText = function()
	{
		$scope.AddLicenseError1 = $translate.instant('AddLicenseError1');
		$scope.AddLicenseError2 = $translate.instant('AddLicenseError2');
		$scope.AddLicenseError3 = $translate.instant('AddLicenseError3');
		$scope.AddLicenseError4 = $translate.instant('AddLicenseError4');
		$scope.AddLicenseError5 = $translate.instant('AddLicenseError5');
		$scope.AddLicenseError6 = $translate.instant('AddLicenseError6');
		$scope.AddLicenseError7 = $translate.instant('AddLicenseError7');	
	    $scope.confirmError = $translate.instant('confirm_error');
		
	}
	
	$scope.getLicenseText();
	
	$scope.license = 
	{
		"title": "",
		"desc" : ""
	}
	
	$scope.uploadedimage = 'img/addphoto.jpg';
	$scope.uploadedimage2 = 'img/addphoto.jpg';
	
	//$scope.uploadedimage2 = 'img/picture.jpg';
	$scope.licenseId = '';
	$scope.serverImage = '';
	$scope.imageSrc = '';
	$scope.image2 = '';
	$scope.showimage = false;
	
	if ($stateParams.ItemId == 0)
	{
		$scope.showDelete = false;
	}
	else
	{
		$scope.showDelete = true;
		
		
		$scope.getLicense = function()
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				license_data = 
				{
					"id" : $stateParams.ItemId,
					"user" : $localStorage.userid
				}					
				$http.post($rootScope.Host+'/get_diving_license.php',license_data).success(function(data)
				{
					$scope.license = data.response;
					
					if (data.response.image)
					{
						$scope.uploadedimage = $rootScope.Host+data.response.image;
					}
					if (data.response.image2)
					{
						$scope.uploadedimage2 = $rootScope.Host+data.response.image2;
					}
					
					$scope.serverImage = data.response.image;
					$scope.serverImage2 = data.response.image2;
					$scope.imageSrc = $scope.uploadedimage;
					$scope.licenseId = data.response.id;
					//console.log($scope.licenses);
				});				
		}
	
		$scope.getLicense();
		
		
	}
	
	

	$scope.CameraOption = function(imagenumber) 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: $scope.AddLicenseError1,
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: $scope.AddLicenseError2,
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1,imagenumber);
		}
	   },
	   {
		text: $scope.AddLicenseError3,
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0,imagenumber);
		}
	   },
	   	   {
		text: $scope.AddLicenseError4,
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   ]
	  });
	  

	  
	 /*
	   var hideSheet = $ionicActionSheet.show({
		 buttons: [
		   { text: 'בחר תמונה מהגלרייה' },
		   { text: 'פתח מצלמה' }
		   //{ text: 'חזור'}
		 ],
		 cancelText: 'ביטול',
		 cssClass: 'social-actionsheet',
		 cancel: function() {
			  return true;
			},
		 buttonClicked: function(index) {
			 if(index<2)
			 	$scope.takePicture(index);
			 
		   return true;
		 }});
			 // For example's sake, hide the sheet after two seconds
	
	*/
	};	
	
	  $scope.OpenCamera = function()
	  {
		  if ($scope.image2 == $scope.uploadedimage2)
		  {
				$ionicPopup.alert({
				 title: $scope.AddLicenseError5,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });				  
		  }
		  else
		  {
			  $scope.CameraOption();
		  }
	  }

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index,imagenumber) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			options.chunkedMode = false;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {
				
				if (imagenumber == 1)
				{
					$scope.image = data.response;
					$scope.uploadedimage = $rootScope.Host+data.response;
				}
				else if (imagenumber == 2)
				{
					$scope.image2 = data.response;
					$scope.uploadedimage2 = $rootScope.Host+data.response;
				}
						
					//$scope.showimage = true;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }
	
	$scope.AddLicenseBtn = function()
	{
		if ($scope.licenseId)
		{
			$scope.Url = 'update_license.php';
		}
		else
		{
			$scope.Url = 'add_license.php';
		}
		//
		if ($scope.image)
		{
			$scope.licenseimage = $scope.image;
		}
		else 
		{
			$scope.licenseimage = $scope.serverImage;
		}

		if ($scope.image2)
		{
			$scope.licenseimage2 = $scope.image2;
		}
		else 
		{
			$scope.licenseimage2 = $scope.serverImage2;
		}
		
		
		if ($scope.license.title =="")
		{
			$ionicPopup.alert({
			 title: $scope.AddLicenseError6,
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				
		}
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			//$scope.licenseimage = ($scope.image !="" ? $scope.image : $scope.serverImage);
		
				
				license_data = 
				{
					"id" : $scope.licenseId,
					"user" : $localStorage.userid,
					"title" : $scope.license.title,
					"desc" : $scope.license.desc,
					"image" : $scope.licenseimage,
					"image2" : $scope.licenseimage2,
					"send" : 1
				}					
				$http.post($rootScope.Host+'/'+$scope.Url,license_data).success(function(data)
				{
					
				});		
			
				$state.go('app.divinglicense');			
		}
		
		

		
	}

	$scope.deleteLicense = function()
	{
		var confirmPopup = $ionicPopup.confirm({
		 title: $scope.AddLicenseError7

	   });
	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				delete_data = 
				{
					"user" : $localStorage.userid,
					"id" : $scope.licenseId
				}					
				$http.post($rootScope.Host+'/del_license.php',delete_data).success(function(data)
				{
					//$scope.getLicenses();
				});
			
			$state.go('app.divinglicense');
		 } 
	   });
 	}

	$scope.ShowImageModal = function(index)
	{
		
		if (index == 1)
		{
			$scope.licenseImageModal = $scope.uploadedimage;
		}
		else if (index == 2)
		{
			$scope.licenseImageModal = $scope.uploadedimage2;
		}
		
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}
	
})


.controller('DivingDiaryCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$translate) {


  $scope.$on('$ionicView.enter', function(e) {
	  
	$scope.approved = 0;
 	$scope.navTitle='<p class="title-image">יומן צלילה</p>';
	$rootScope.currentPage = 'divingdiary';
	$scope.DiveCount = $rootScope.DivesCount;
	$scope.phpHost = $rootScope.Host;
	//$ionicSideMenuDelegate.canDragContent(false);
	
	if(window.cordova){
		window.ga.trackView("יומן צלילה ראשי");
	}	

	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
		 $scope.DivingDiaryImage1 = $rootScope.LanguagesJson[14][$rootScope.DefaultLanguage];
		 $scope.DivingDiaryImage2 = $rootScope.LanguagesJson[15][$rootScope.DefaultLanguage];			   
	   }

    });

	 
	 $scope.DivingDiaryError1 = $translate.instant('AddLicenseError7');
	 
	
		$scope.getDiary = function()
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				diary_data = 
				{
					"user" : $localStorage.userid
				}					
				$http.post($rootScope.Host+'/get_diving_diary.php',diary_data).success(function(data)
				{
					$scope.licenses = data.response;
					$rootScope.DivesCount = data.response.length+1;
					//alert ($scope.DiveCount)
					console.log("Diary")
					console.log($scope.licenses);
				});				
		}
	
		$scope.getDiary();
	
	
	
	$scope.AddDiaryBtn = function()
	{
		//$state.go('app.addlisense');
		$state.go('app.adddiary', { ItemId: 0,Count: -1 });
	}
	$scope.GearCheckBtn = function()
	{
		$state.go('app.gearscheck');
	}
	
	$scope.sortArray = function(id)
	{
		if (id == 1)
		{
			$scope.approved  = 0;
		}
		else
		{
			$scope.approved =  1;
		}
	}
	


	$scope.deleteDiary = function(id,title)
	{
		var confirmPopup = $ionicPopup.confirm({
		 title: $scope.DivingDiaryError1

	   });
	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				delete_data = 
				{
					"user" : $localStorage.userid,
					"id" : $stateParams.ItemId
				}					
				$http.post($rootScope.Host+'/del_diary.php',delete_data).success(function(data)
				{
					$scope.getDiary();
				});
		 } 
	   });
	}

});	
})

.controller('ClubsCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$ionicModal,$translate,$timeout,$ionicScrollDelegate) {
	
	
	$scope.params = 
	{
		"selectedcountry" : "IL",
		"selectedclub" : ""
	}

		
	//$scope.$on('$ionicView.enter', function(e) 
	//{
		$scope.phpHost = $rootScope.Host;
		
	
		if(window.cordova){
			window.ga.trackView("מועדני צלילה ראשי");
		}

		$scope.scrollUp = function()
		{
			$ionicScrollDelegate.scrollTop(true);
		}		
		
		$scope.messageclub = 
		{
			"details" : ""
		}
		
		$scope.ImgHost = $rootScope.Host;
		$rootScope.currentPage = 'clubs';

		$rootScope.$watch("LanguagesJson", function(){
		   
		   if ($rootScope.LanguagesJson.length > 0)
		   {
			 $scope.ClubsImage1 = $rootScope.LanguagesJson[16][$rootScope.DefaultLanguage];
			 $scope.ClubsImage2 = $rootScope.LanguagesJson[17][$rootScope.DefaultLanguage]; 	
			 $scope.ClubsImage3 = $rootScope.LanguagesJson[18][$rootScope.DefaultLanguage]; 			   
		   }

		});


		
		

		 $scope.ClubsError1 = $translate.instant('ClubsError1');
		 $scope.ClubsError2 = $translate.instant('ClubsError2');
		 $scope.ClubsError3 = $translate.instant('ClubsError3');
		 $scope.confirmError = $translate.instant('confirm_error');
		 
		//$ionicSideMenuDelegate.canDragContent(false);
		
			$scope.getClubs = function()
			{
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
				
					clubs_data = 
					{
						"user" : $localStorage.userid
					}					
					$http.post($rootScope.Host+'/get_clubs.php',clubs_data).success(function(data)
					{
						$scope.clubs = data.response;
						$rootScope.ClubsDataArray = $scope.clubs;
						console.log("clubs: ", $scope.clubs);
					});				
			}
		
			$scope.getClubs();
		
			$scope.SendDetailsBtn = function(id)
			{
				
				$rootScope.sendClub = id;
				
		
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
				
					send_data = 
					{
						"user" : $localStorage.userid,
						"clubid" : id
					}					
					$http.post($rootScope.Host+'/send_club.php',send_data).success(function(data)
					{
					});		
					
					$ionicPopup.alert({
					 title: $scope.ClubsError1,
					buttons: [{
					text: $scope.confirmError,
					type: 'button-positive',
					}]
				   });		
				
					$state.go('app.main');
			//	}
			}
			
			$scope.SendClubMessage = function(id,clubname)
			{
				
			$ionicModal.fromTemplateUrl('templates/club-message.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(modal) {
			  $scope.modal = modal;
			  $scope.modal.show();
			 });
			
			$scope.sendMessage = function()
			{
				if (!$scope.messageclub.details)
				{
						$ionicPopup.alert({
						 title: $scope.ClubsError2,
						buttons: [{
						text: $scope.confirmError,
						type: 'button-positive',
						}]
					   });						
				}
				else 
				{
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
					
						send_data = 
						{
							"user" : $localStorage.userid,
							"clubid" : id,
							"clubname" : clubname,
							"message" :  $scope.messageclub.details
						}					
						console.log(send_data)
						$http.post($rootScope.Host+'/send_club_message.php',send_data).success(function(data)
						{
						});		
						
						$ionicPopup.alert({
						 title: $scope.ClubsError3,
						buttons: [{
						text: $scope.confirmError,
						type: 'button-positive',
						}]
					   });		
						
						$scope.messageclub.details = '';
						$scope.modal.hide();
						//$state.go('app.clubs');				
				}
			

			//	}
			
			}
			 

			}
			
		$scope.checkSimCountry = function()
		{
			if ($rootScope.UserCountryCode)
			{
				for (i = 0; i < $rootScope.CountryClubsArray.length; i++) 
				{
					if ($rootScope.CountryClubsArray[i].value == $rootScope.UserCountryCode)
					{
						$scope.params.selectedcountry = $rootScope.UserCountryCode;
					}
				}
			}
		}
		
		$scope.checkSimCountry();
			
		$scope.languageSelect = function()
		{
			// Prepare the picker configuration
			var config = {
				title: "בחירת מדינה", 
				items: $rootScope.CountryClubsArray,
				selectedValue: $scope.params.selectedcountry,
				doneButtonLabel: "Done",
				cancelButtonLabel: "Cancel"
			};

			// Show the picker
			window.plugins.listpicker.showPicker(config, 
				function(item) { 
				
					 $timeout(function() {
						$scope.params.selectedcountry = String(item);
					}, 300);

					
					//alert("You have selected " + item);
				},
				function() { 
					//alert("You have cancelled");
				}
			);			
		}	

		
		$scope.closeMessageModal= function()
		{
			$scope.modal.hide();
		}
		
		$scope.removeEmpty = function(data)
		{
			if (data.id)
			{
				return true
			}
			else
			{
				return false;
			}
		}

	//});	
})


.controller('ClubsDetailsCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$ionicModal,$translate) {
	
	$scope.phpHost = $rootScope.Host;
	$scope.clubIndex = $stateParams.IndexId;
	$scope.ClubId = $stateParams.ClubId;
	
	for (i = 0; i < $rootScope.ClubsDataArray.length; i++) 
	{ 
		if ($rootScope.ClubsDataArray[i].id == $scope.ClubId)
			$scope.clubData = $rootScope.ClubsDataArray[i];
	}
	
	if ($scope.clubData.club_gallery.length > 1)
		$scope.showPager = true;
	else
		$scope.showPager = false;

					
	
	
	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("מסך פרטי מועדון");
		}
	});
	
	
	console.log("club data: ",$scope.clubData)
	
	$scope.openFacebook = function()
	{
		if ($scope.clubData.facebook)
			iabRef = window.open($scope.clubData.facebook, '_blank', 'location=yes','toolbar=no');
		else
		{
			$ionicPopup.alert({
			 title: $translate.instant('nofbclubtext'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });	
		}
	}
	
	$scope.openWaze = function()
	{
		if ($scope.clubData.location_lat)
			window.location = "waze://?q="+$scope.clubData.location_lat+","+$scope.clubData.location_lng+"&navigate=yes";
		else
		{
			$ionicPopup.alert({
			 title: $translate.instant('nowazeclubtext'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				
		}
	}
	
	$scope.openWebsite = function()
	{
		if ($scope.clubData.website)
			iabRef = window.open($scope.clubData.website, '_blank', 'location=yes','toolbar=no');
		else
		{
			$ionicPopup.alert({
			 title: $translate.instant('nowebsiteclubtext'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				
		}
	}
	
	$scope.PhoneClub = function()
	{
		if ($scope.clubData.phone2)
			window.location.href="tel://"+$scope.clubData.phone2;
		else
		{
			$ionicPopup.alert({
			 title: $translate.instant('nophoneclubtext'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				
		}
	}
	
	$scope.MailClub = function()
	{
		if ($scope.clubData.email)
			window.location = "mailto:"+$scope.clubData.email;	
		else
		{
			$ionicPopup.alert({
			 title: $translate.instant('noemailclubtext'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				
		}
	}
	
	$scope.ShareClub = function()
	{
		window.plugins.socialsharing.share($scope.clubData.name+' '+$scope.clubData.phone , 'I LOVE DIVE', '', null);		
	}

})

.controller('AddDiaryCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$cordovaCamera,$timeout,$ionicModal,dateFilter,$cordovaDatePicker,$translate) {
	
	$scope.SendToClub = 0;
	$scope.clubname = '';
	$scope.personalid = $localStorage.personalid;
	
	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("הוספת יומן צלילה");
		}
	});	

	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
		 $scope.AddDiaryImage1 = $rootScope.LanguagesJson[19][$rootScope.DefaultLanguage];
		 $scope.AddDiaryImage2 = $rootScope.LanguagesJson[20][$rootScope.DefaultLanguage]; 	
		 $scope.AddDiaryImage3 = $rootScope.LanguagesJson[21][$rootScope.DefaultLanguage]; 
		 $scope.AddDiaryImage4 = $rootScope.LanguagesJson[22][$rootScope.DefaultLanguage]; 			   
	   }

    });

	 $scope.AddDiaryError1 = $translate.instant('AddLicenseError1');
	 $scope.AddDiaryError2 = $translate.instant('AddLicenseError2');
	 $scope.AddDiaryError3 = $translate.instant('AddLicenseError3');
	 $scope.AddDiaryError4 = $translate.instant('AddLicenseError4');

	 $scope.AddDiaryError5 = $translate.instant('AddDiaryError1');
	 $scope.AddDiaryError6 = $translate.instant('AddDiaryError2');
	 $scope.AddDiaryError7 = $translate.instant('AddDiaryError3');
	 $scope.AddDiaryError8 = $translate.instant('AddDiaryError4');
	 $scope.AddDiaryError9 = $translate.instant('AddDiaryError5');
	 $scope.AddDiaryError10 = $translate.instant('AddDiaryError6');
	 $scope.AddDiaryError11 = $translate.instant('AddDiaryError7');
	 $scope.AddDiaryError12 = $translate.instant('AddDiaryError8');
	 $scope.AddDiaryError13 = $translate.instant('AddDiaryError9');
	 $scope.AddDiaryError14 = $translate.instant('AddDiaryError10');
	 $scope.AddDiaryError15 = $translate.instant('AddDiaryError11');
	 $scope.AddDiaryError16 = $translate.instant('AddDiaryError12');
	 $scope.AddDiaryError17 = $translate.instant('AddDiaryError13');

	 $scope.confirmError = $translate.instant('confirm_error');


	if ($rootScope.DefaultLanguage =="he")
	{
		$scope.languagesJson = $rootScope.HebrewCountries;
	}
	else
	{
		$scope.languagesJson = $rootScope.EnglishCountries;
	}

	$scope.diary = 
	{
		"clubname": "",
		"divedate": "",		
		"depth" : "",
		"type" : "",
		"country" : "",
		"time" : "",
		"site" : "",
		"weights" : "",
		"steel" : "",
		"aluminum" : "",
		"suit" : "",
		"air_start" : "",
		"air_finsh" : "",
		"partner" : "",
		"coucher" : "",
		"bottom_time" : "0",
		"total_time" : "",
		"freetext" : "",
		"personalid" : $scope.personalid
	}
	
	if ($stateParams.Count == -1)
	{
		$scope.diarynumber = $rootScope.DivesCount;
	}
	else
	{
		$scope.diarynumber = "";
		//$scope.diarynumber = parseInt($stateParams.Count)+1;
	}
	
	$scope.isIdRequired = function (){
		if ($scope.diary.clubname != '' && $scope.ClubsCountriesArray){
            var club = '';
            for (var l = 0; l < $scope.ClubsCountriesArray.length; l++){
                for (var k = 0; k < $scope.ClubsCountriesArray[l].clubs_array.length; k++){
                    if ($scope.ClubsCountriesArray[l].clubs_array[k].index == $scope.diary.clubname){
                        club = $scope.ClubsCountriesArray[l].clubs_array[k];
                    }
                }
            }

            if (club.id_required == '1' && $scope.diary.personalid == ''){
                return true;
            }
		}

		return false;
	};
	
	$rootScope.currentPage = 'adddiary';
	$scope.editdisabled = false;
	$scope.imageSrc = '';
	$scope.uploadedimage = 'img/addphoto.png';
	$scope.uploadedimage2 = 'img/addphoto.png';
	//$scope.uploadedimage2 = 'img/picture.jpg';
	$scope.divingImage = '';
	$scope.clubcode = '';
	$scope.DiaryId = '';
	
	
	
	var dtToday = new Date();
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();

    $scope.maxDate =   day + '-' + month + '-' + year;    
	//alert ($scope.maxDate)
	
	$scope.CameraOption = function(picturenumber) 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: $scope.AddDiaryError1,
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: $scope.AddDiaryError2,
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1,picturenumber);
		}
	   },
	   {
		text: $scope.AddDiaryError3,
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0,picturenumber);
		}
	   },
	   	   {
		text: $scope.AddDiaryError4,
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   ]
	  });
	
	};	
	
	  $scope.OpenCamera = function(index)
	  {
		$scope.CameraOption(index);
	  }

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index,picturenumber) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, // Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
		/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
		*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			options.chunkedMode = false;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {
				
					if (picturenumber == 1)
					{
						$scope.image = data.response;
						$scope.uploadedimage = $rootScope.Host+data.response;
					}
					else if (picturenumber == 2)
					{
						$scope.image2 = data.response;
						$scope.uploadedimage2 = $rootScope.Host+data.response;
					}
					
					$scope.showimage = true;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }
	
	$scope.getClubsByCountry = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		clubs_data = 
		{
			"user" : $localStorage.userid
		}					
		$http.post($rootScope.Host+'/get_clubs_by_country.php',clubs_data).success(function(data)
		{
			$scope.ClubsCountriesArray = data;
			console.log("ClubsCountriesArray",data);

		});				
	}
	
	$scope.getClubsByCountry();
	
		$scope.getClubs = function()
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				clubs_data = 
				{
					"user" : $localStorage.userid
				}					
				$http.post($rootScope.Host+'/get_clubs.php',clubs_data).success(function(data)
				{
					$scope.clubs = data.response;
					
					console.log($scope.clubs);
					/*
					for (i = 0; i < $scope.clubs.length; i++) 
					{ 
						if ($rootScope.sendClub == $scope.clubs[i].id)
						{
							$scope.diary.clubname = i;	
							
						}

					}
					*/
					
					if ($stateParams.Count == -1 && $rootScope.sendClub)
					{
						 $timeout(function()
						{
							$scope.diary.clubname = $rootScope.sendClub;
						}, 300);						
					}

				});				
		}
	


		$scope.getClubs();
		
		
		$scope.getDiary = function()
		{
			
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				license_data = 
				{
					"id" : $stateParams.ItemId,
					"user" : $localStorage.userid
				}					
				$http.post($rootScope.Host+'/get_diving_diary.php',license_data).success(function(data)
				{
					//alert (data.response.clubname)
					//$scope.diary = data.response;
					$scope.DiaryId = data.response.id;
					
					 $timeout(function() {
						//$scope.diary.clubname = data.response.clubname;
						$scope.diary = data.response;

					}, 300);	
				
					$scope.diarynumber = parseInt(data.response.dive_num);
					$scope.diary.depth = parseInt(data.response.depth);
					//$scope.diary.time = parseInt(data.response.time);
					$scope.diary.divedate = data.response.divedate;
					$scope.diary.air_start = parseInt(data.response.air_start);
					$scope.diary.air_finsh = parseInt(data.response.air_finsh);
					$scope.diary.bottom_time = parseInt(data.response.bottom_time);
					
					if(isNaN($scope.diary.bottom_time))
					$scope.diary.bottom_time = 0;
					
					$scope.diary.total_time = parseInt(data.response.total_time);					
					$scope.divingImage = data.response.image;
					$scope.divingImage2 = data.response.image2;
					$scope.imageSrc = $rootScope.Host+$scope.divingImage;
					if (data.response.image)
					{
						$scope.uploadedimage = $rootScope.Host+$scope.divingImage;
					}
					if (data.response.image2)
					{
						$scope.uploadedimage2 = $rootScope.Host+$scope.divingImage2;
					}
					
					
					
					if (data.response.status == 1)
					{
						$scope.editdisabled = true;
					}
					
					//alert ($scope.DiaryId)
					//$scope.uploadedimage = $rootScope.Host+data.response.image;
					//console.log($scope.licenses);
				});				
		}
	
	$scope.setClubName = function(clubname)
	{
		$scope.diary.clubname = clubname;
		//alert ($scope.clubname);
	}
	
	
	if ($stateParams.ItemId == 0)
	{
		$scope.edit = false;
		$scope.diaryTitle = "<p class='title-image'>"+$scope.AddDiaryError5+"</p>";
		$scope.diary.divedate = $scope.maxDate;
	}
	else
	{
		
		$scope.edit = true;
		$scope.diaryTitle = "<p class='title-image'>"+$scope.AddDiaryError6+"</p>";
		$scope.getDiary();
		
	}
	


		
	
	$scope.SaveBtn = function(type)
	{	
	
		if(type == 2)
		$scope.SendToClub = 1;
		
		if ($scope.diary.clubname == "")
		{
				$ionicPopup.alert({
				 title: $scope.AddDiaryError7,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });				
		}
		else if ($scope.diary.divedate == "")
		{
				$ionicPopup.alert({
				 title: $scope.AddDiaryError8,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });				
		}
		else if ($scope.diary.type == "")
		{
				$ionicPopup.alert({
				 title: $scope.AddDiaryError9,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });				
		}
		else if ($scope.diary.depth == "")
		{
				$ionicPopup.alert({
				 title: $scope.AddDiaryError10,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });			
		   
		}
		else if ($scope.diary.bottom_time == "")
		{
				$ionicPopup.alert({
				 title: $scope.AddDiaryError11,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });			
		}
		else if ($scope.isIdRequired() && $scope.diary.personalid == "")
        {
            $ionicPopup.alert({
                title: $scope.AddDiaryError17,
                buttons: [{
                    text: $scope.confirmError,
                    type: 'button-positive',
                }]
            });
        }
		
		else
		{
			if (type == 1)
			{
				
				for (i = 0; i < $scope.clubs.length; i++) 
				{ 
					if ($scope.diary.clubname == $scope.clubs[i].id)
					{
						$scope.clubcode = $scope.clubs[i].clubcode;
					}

				}
				
				
				 $ionicPopup.prompt({
				   title: $scope.AddDiaryError12,
				  // template: 'הזן קוד מועדון:',
				   inputType: 'text',
				   inputPlaceholder: $scope.AddDiaryError13
				 }).then(function(res) {
					if (res)
					{
					   if ($scope.clubcode == res)
					   {
						   $scope.sendData(1);
					   }				   
					   else
					   {
						   //$scope.SaveBtn(1);
						   
							$ionicPopup.alert({
							 title: $scope.AddDiaryError14,
							buttons: [{
							text: 'אשר',
							type: 'button-positive',
							}]
						   });   
						   
						   
					   }	   
					}
				 });
			}
			
			else 
				
				{
					$scope.sendData();
				}
			
		
		}

	}
	
	
	$scope.sendData = function(approved)
	{
	if (approved == 1)
	{
		$scope.approved = 1;
	}
	else
	{
		$scope.approved = '';
	}
	
	if ($scope.image)
	{
		$scope.newimage = $scope.image;
	}
	else
	{
		$scope.newimage = $scope.divingImage;
	}
	
	if ($scope.image2)
	{
		$scope.newimage2 = $scope.image2;
	}
	else
	{
		$scope.newimage2 = $scope.divingImage2;
	}

	
	
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			
			if ($scope.DiaryId)
			{
				$scope.Url = 'update_diary.php';
				$scope.message = $scope.AddDiaryError15;
			}
			else
			{
				$scope.Url = 'add_diary.php';
				$scope.message = $scope.AddDiaryError16;
			}

			$scope.newdiarydate = dateFilter($scope.diary.divedate, 'dd-MM-yyyy');
			
				send_data = 
				{
					"id" : $scope.DiaryId,
					"user" : $localStorage.userid,
					"clubname" : $scope.diary.clubname,
					"type" : $scope.diary.type,
					"country" : $scope.diary.country,
					"depth" : $scope.diary.depth,
					"time" : $scope.diary.time,
					"site" : $scope.diary.site,
					"weights" : parseInt($scope.diary.weights),
					"steel" : $scope.diary.steel,
					"aluminum" : $scope.diary.aluminum,
					"suit" : $scope.diary.suit,	
					"air_start" : parseInt($scope.diary.air_start),	
					"air_finsh" : parseInt($scope.diary.air_finsh),	
					"partner" : $scope.diary.partner,
					"coucher" : $scope.diary.coucher,
					"freetext" : $scope.diary.freetext,					
					"bottom_time" : parseInt($scope.diary.bottom_time),	
					"total_time" : $scope.diary.total_time,					
					"approved" : $scope.approved,	
					"diarydate" : $scope.newdiarydate,		
					"image" : $scope.newimage,						
					"image2" : $scope.newimage2,						
					"send" : 1,
					"SendToClub" : $scope.SendToClub
				}

				if ($scope.isIdRequired()){
					send_data.personalid = $scope.diary.personalid;
                    $localStorage.personalid = $scope.diary.personalid;
				}

				console.log(send_data)

				$http.post($rootScope.Host+'/'+$scope.Url,send_data).success(function(data)
				{
				});		
				
				$ionicPopup.alert({
				 title: $scope.message,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });	
			   
		
		$state.go('app.divingdiary');
		
	}
	

	$scope.ShowImageModal = function(index)
	{
		if (index == 1)
		{
			$scope.diaryImageModal = $scope.uploadedimage;
		}
		else if (index == 2)
		{
			$scope.diaryImageModal = $scope.uploadedimage2;
		}
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}	

})


.controller('InsuranceCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$ionicHistory,$translate,$timeout,$cordovaCamera,$ionicModal) {
$rootScope.currentPage = 'insurance';

//$ionicNavBarDelegate.showBackButton(title);
	/*
	 $ionicHistory.nextViewOptions({
		disableBack: false
	  });
	 */
	 
	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("מסך רכישת ביטוח");
		}
	});	 
	 
	$scope.navTitle = 'רישיון צלילה ראשי';

	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
		 $scope.MainLicenseImage1 = $rootScope.LanguagesJson[8][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage2 = $rootScope.LanguagesJson[9][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage3 = $rootScope.LanguagesJson[10][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage4 = $rootScope.LanguagesJson[11][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage5 = $rootScope.LanguagesJson[12][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage6 = $rootScope.LanguagesJson[13][$rootScope.DefaultLanguage];
		 $scope.MainLicenseImage7 = $rootScope.LanguagesJson[27][$rootScope.DefaultLanguage];
		 $scope.MainLicenseImage8 = $rootScope.LanguagesJson[28][$rootScope.DefaultLanguage];

		$scope.InsuranceImage1 = $rootScope.LanguagesJson[29][$rootScope.DefaultLanguage];
		$scope.InsuranceImage2 = $rootScope.LanguagesJson[30][$rootScope.DefaultLanguage];		 
		//alert ($scope.InsuranceImage2)
	   }

    });



	 $scope.MainLicenseError1 = $translate.instant('AddLicenseError1');
	 $scope.MainLicenseError2 = $translate.instant('AddLicenseError2');
	 $scope.MainLicenseError3 = $translate.instant('AddLicenseError3');
	 $scope.MainLicenseError4 = $translate.instant('AddLicenseError4');
	 $scope.MainLicenseError5 = $translate.instant('MainLicenseError1');	 	 	 
	 $scope.confirmError = $translate.instant('confirm_error');		   

	 $scope.insuranceText1 = $translate.instant('insurance_delete');
	 $scope.insuranceText2 = $translate.instant('insurance_updated');		 
	 
	 $scope.externalInsurance = function(type)
	 {
		 if (type == 1)
		 {
			iabRef = window.open('http://insurance.idive.co.il/Insurance/Buy?agentid=21621','_blank', 'location=yes');
			 
		 }
		 else
		 {
			 iabRef = window.open('http://insurance.idive.co.il/Insurance/Search','_blank', 'location=yes');
		 }
		
	 }
	 

		$scope.getInsurance = function()
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				license_data = 
				{
					"user" : $localStorage.userid
				}					
				$http.post($rootScope.Host+'/get_insurance.php',license_data).success(function(data)
				{
				    console.log("ResponseImage : " , $rootScope.Host+data.response.image)
					if (data.response.image)
					{
						$scope.uploadedimage = $rootScope.Host+data.response.image;
						$scope.serverImage = data.response.image;
					}
					else
					{
						$scope.uploadedimage = 'img/addphoto.png';
						$scope.serverImage = "";					
					}


				});				
		}
	
		$scope.getInsurance();	 

	
	$scope.CameraOption = function(imagenumber) 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: $scope.MainLicenseError1,
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: $scope.MainLicenseError2,
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1,imagenumber);
		}
	   },
	   {
		text: $scope.MainLicenseError3,
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0,imagenumber);
		}
	   },
	   	   {
		text: $scope.MainLicenseError4,
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   ]
	  });
	  

	

	};	
	
 

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index,imagenumber) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
		/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
		*/
		    console.log("sendImage : " , imageData)
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			options.chunkedMode = false;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			//alert(JSON.stringify(data));
            console.log("sendImage1 : " , data)
			 $timeout(function() {
				if (data.response) {
                    console.log("sendImage2 : " , data.response)
				if (imagenumber == 1)
				{
					$scope.image = data.response;
					$scope.uploadedimage = $rootScope.Host+data.response;
				}
				else if (imagenumber == 2)
				{
					$scope.image2 = data.response;
					$scope.uploadedimage2 = $rootScope.Host+data.response;
				}
                    window.location = "#/app/insurance";
					//$scope.showimage = true;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }
	
	 
	$scope.AddLicenseBtn = function()
	{
		
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			//$scope.licenseimage = ($scope.image !="" ? $scope.image : $scope.serverImage);
		
			if ($scope.image)
			{
				$scope.licenseimage = $scope.image;
			}
			else
			{
				$scope.licenseimage = $scope.serverimage;
			}
			
			/*
			if ($scope.image2)
			{
				$scope.licenseimage2 = $scope.image2;
				
			}
			else
			{
				$scope.licenseimage2 = $scope.serverimage2;
			}
			*/
				
				update_data = 
				{
					"user" : $localStorage.userid,
					"image" : $scope.licenseimage,
				//	"image2" : $scope.licenseimage2,
					"send" : 1
				}					
				$http.post($rootScope.Host+'/update_insurance.php',update_data).success(function(data)
				{
					
				});	

				$ionicPopup.alert({
				title: $scope.insuranceText2,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });
			   
				$state.go('app.divinglicense');	
	}
	
	$scope.ShowImageModal = function(index)
	{
		
		if (index == 1)
		{
			$scope.licenseImageModal = $scope.uploadedimage;
		}
		else if (index == 2)
		{
			$scope.licenseImageModal = $scope.uploadedimage2;
		}
		
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
 
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}  
	
	
	$scope.deleteLicense = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

	
		send_data = 
		{
			"user" : $localStorage.userid				
		}					
		$http.post($rootScope.Host+'/delete_insurance.php',send_data).success(function(data)
		{			
			$ionicPopup.alert({
			title: $scope.insuranceText1,
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });	
			window.location = "#/app/divinglicense";
	
		});

	}

	
})

.controller('GearsCheckMainCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,dateFilter,$timeout,$cordovaCamera,$ionicModal,$translate) {

	$scope.phpHost = $rootScope.Host;
	$scope.ActiveTab = $rootScope.defaultGearsTab;

	
	$scope.setActiveTab = function(tab)
	{
		$scope.ActiveTab = tab;
		$rootScope.defaultGearsTab = tab;
	}
	
	$scope.getUserGears = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

	
		send_data = 
		{
			"user" : $localStorage.userid				
		}					
		$http.post($rootScope.Host+'/get_user_gears.php',send_data).success(function(data)
		{			
			console.log("user gears: " , data)
			$rootScope.UserGearArray = data;
		});		
	}
	
	$scope.getUserGears();
	
	$scope.deleteGear = function(index,id)
	{
		send_data = 
		{
			"user" : $localStorage.userid,
			"id" : id,
		}					
		$http.post($rootScope.Host+'/delete_user_gears.php',send_data).success(function(data)
		{			
			$rootScope.UserGearArray.splice(index, 1);
		});
	}

})

.controller('ManageGearsCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,dateFilter,$timeout,$cordovaCamera,$ionicModal,$translate) {

	$scope.uploadedimage = 'img/addphoto.png';
	
	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("הוספה/עריכת ציוד תקופתי");
		}
	});		


	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
			$scope.GearsCheckImage1 = $rootScope.LanguagesJson[23][$rootScope.DefaultLanguage];
			$scope.GearsCheckImage2 = $rootScope.LanguagesJson[24][$rootScope.DefaultLanguage]; 		
	   }

    });

	 $scope.GearsError1 = $translate.instant('AddLicenseError1');
	 $scope.GearsError2 = $translate.instant('AddLicenseError2');
	 $scope.GearsError3 = $translate.instant('AddLicenseError3');
	 $scope.GearsError4 = $translate.instant('AddLicenseError4');
	 $scope.GearsError5 = $translate.instant('GearsError1');	 	 	 
	 $scope.confirmError = $translate.instant('confirm_error');
	 

	$scope.fields = 
	{
		"gearid" : "",
		"geartype": "",
		"geardate": "",
		"remarks" : "",
		"image" : ""
	}
	
	$scope.GearId = $stateParams.ItemId;
	
	if ($scope.GearId != -1)
	{
		$scope.fields = $rootScope.UserGearArray[$scope.GearId];
		if ($scope.fields.image)
			$scope.uploadedimage = $rootScope.Host+$scope.fields.image
	}
	
	
	
	$scope.SaveGearBtn = function()
	{
		if ($scope.fields.geartype =="")
		{
			$ionicPopup.alert({
			 title: $translate.instant('savegearspicktype'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				
		}
		else if ($scope.fields.geardate =="" )
		{
			$ionicPopup.alert({
			 title: $translate.instant('savegearspickdate'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				
		}

		else 
		{
			$scope.gearnewdate = dateFilter($scope.fields.geardate, 'dd-MM-yyyy');
			
			if ($scope.gearsnewimage)
				$scope.newimage = $scope.gearsnewimage;
			else
				$scope.newimage = $scope.fields.image;

		
			send_data = 
			{
				"user" : $localStorage.userid,
				"gearid" : $scope.fields.gearid,
				"geardate" : $scope.fields.geardate,
				"remarks" : $scope.fields.remarks,
				"geartype" : $scope.fields.geartype,
				"image" : $scope.newimage,
				"send" : 1			
			}
			//console.log(send_data)	
			$http.post($rootScope.Host+'/save_user_gear.php',send_data).success(function(data)
			{
			});		
			
			$ionicPopup.alert({
			 title: $scope.GearsError5,
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });	
			   
		
			$state.go('app.gearscheck');


		
		}
		
	}


	$scope.CameraOption = function(imagenumber) 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: $scope.GearsError1,
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: $scope.GearsError2,
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1,imagenumber);
		}
	   },
	   {
		text: $scope.GearsError3,
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0,imagenumber);
		}
	   },
	   	   {
		text: $scope.GearsError4,
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   ]
	  });
	
	};	
	

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index,imagenumber) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
		/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
		*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			options.chunkedMode = false;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {
				
					$scope.gearsnewimage = data.response;
					$scope.uploadedimage = $rootScope.Host+data.response;	
				
				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }

	
	
	$scope.ShowImageModal = function(index)
	{
		
		$scope.gearsmodelimage = $scope.uploadedimage;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}

	
})

//
.controller('GearsPrimaryCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,dateFilter,$timeout,$cordovaCamera,$ionicModal,$translate) {

		$scope.navTitle='<p class="title-image">בדיקת ציוד תקופתית</p>';
		$scope.uploadedimage1 = 'img/addphoto.png';
		$scope.uploadedimage2 = 'img/addphoto.png';
		$scope.uploadedimage3 = 'img/addphoto.png';
		
		$scope.$on('$ionicView.enter', function(e) {
			if(window.cordova){
				window.ga.trackView("מסך ציוד תקופתי");
			}
		});		


	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
			$scope.GearsCheckImage1 = $rootScope.LanguagesJson[23][$rootScope.DefaultLanguage];
			$scope.GearsCheckImage2 = $rootScope.LanguagesJson[24][$rootScope.DefaultLanguage]; 
		
			//alert ($scope.GearsCheckImage1)
			//alert ($scope.GearsCheckImage2)
	   }

    });

	 $scope.GearsError1 = $translate.instant('AddLicenseError1');
	 $scope.GearsError2 = $translate.instant('AddLicenseError2');
	 $scope.GearsError3 = $translate.instant('AddLicenseError3');
	 $scope.GearsError4 = $translate.instant('AddLicenseError4');
	 $scope.GearsError5 = $translate.instant('GearsError1');	 	 	 
	 $scope.confirmError = $translate.instant('confirm_error');
	 
	 

	 
		//$scope.uploadedimage2 = 'img/picture.jpg';
		$rootScope.currentPage = 'gearscheck';
		$scope.oldimage1 = '';
		$scope.oldimage2 = '';
		$scope.oldimage3 = '';
		
		$scope.fields = 
		{
			"BalanceDate": "",
			"BalanceRemarks": "",
			"BreathingDate" : "",
			"BreathingRemarks" : "",
			"ContainerDate" : "",
			"ContainerRemarks" : ""			
		}
		

	$scope.CameraOption = function(imagenumber) 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: $scope.GearsError1,
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: $scope.GearsError2,
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1,imagenumber);
		}
	   },
	   {
		text: $scope.GearsError3,
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0,imagenumber);
		}
	   },
	   	   {
		text: $scope.GearsError4,
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   ]
	  });
	
	};	
	

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index,imagenumber) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
		/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
		*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			options.chunkedMode = false;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {
				
					if (imagenumber ==1)
					{
						$scope.image1 = data.response;
						$scope.uploadedimage1 = $rootScope.Host+data.response;						
					}
					else if (imagenumber ==2)
					{
						$scope.image2 = data.response;
						$scope.uploadedimage2 = $rootScope.Host+data.response;					
					}
					else if (imagenumber ==3)
					{
						$scope.image3 = data.response;
						$scope.uploadedimage3 = $rootScope.Host+data.response;						
					}
				
				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }
			
		$scope.changeDate = function(dt)
		{
			$scope.GearDate = dateFilter(dt, 'dd-MM-yyyy');
		}

		$scope.getGears = function()
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				license_data = 
				{
					"user" : $localStorage.userid
				}					
				$http.post($rootScope.Host+'/get_gears.php',license_data).success(function(data)
				{
					$scope.fields = data.response;
					
					if (data.response.BalanceImage)
					{
						$scope.uploadedimage1  =  $rootScope.Host+data.response.BalanceImage;
						$scope.oldimage1 = data.response.BalanceImage;
					}
					if (data.response.BreathingImage)
					{
						$scope.uploadedimage2  =  $rootScope.Host+data.response.BreathingImage;
						$scope.oldimage2 = data.response.BreathingImage;
					}
					if (data.response.ContainerImage)
					{
						$scope.uploadedimage3  =  $rootScope.Host+data.response.ContainerImage;
						$scope.oldimage3 = data.response.ContainerImage;
					}					
					//$scope.fields.BalanceDate = data.response.BalanceDate;
					//$scope.fields.BalanceRemarks = data.response.BalanceRemarks;
					//$scope.fields.BreathingDate = data.response.BreathingDate;
					//$scope.fields.BreathingRemarks = data.response.BreathingRemarks;
					//console.log($scope.licenses);
				});				
		}
	
		$scope.getGears();
		
		

	$scope.SaveBtn = function()
	{
			//alert ($scope.fields.BalanceDate)
	
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			$scope.BalanceDate = dateFilter($scope.fields.BalanceDate, 'dd-MM-yyyy');
			$scope.BreathingDate = dateFilter($scope.fields.BreathingDate, 'dd-MM-yyyy');
			$scope.ContainerDate = dateFilter($scope.fields.ContainerDate, 'dd-MM-yyyy');

	
	
			
			if ($scope.image1)
				$scope.balanceimage = $scope.image1;
			else
				$scope.balanceimage = $scope.oldimage1;
			//
			if ($scope.image2)
				$scope.breathingimage = $scope.image2;
			else
				$scope.breathingimage = $scope.oldimage2;
			//
			if ($scope.image3)
				$scope.containerimage = $scope.image3;
			else
				$scope.containerimage = $scope.oldimage3;

			
				send_data = 
				{
					"user" : $localStorage.userid,
					"BalanceDate" : $scope.BalanceDate,
					"BalanceRemarks" : $scope.fields.BalanceRemarks,
					"BalanceImage" : $scope.balanceimage,
					"BreathingDate" : $scope.BreathingDate,
					"BreathingRemarks" : $scope.fields.BreathingRemarks,
					"BreathingImage" : $scope.breathingimage,
					"ContainerDate" : $scope.ContainerDate,
					"ContainerRemarks" : $scope.fields.ContainerRemarks,
					"ContainerImage" : $scope.containerimage,
					"send" : 1			
				}
				//console.log(send_data)	
				$http.post($rootScope.Host+'/update_gear.php',send_data).success(function(data)
				{
				});		
				
				$ionicPopup.alert({
				 title: $scope.GearsError5,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });	
			   
		
		$state.go('app.gearscheck');
	}

	$scope.ShowImageModal = function(index)
	{
		
		if (index == 1)
			$scope.gearsmodelimage = $scope.uploadedimage1;

		else if (index == 2)
			$scope.gearsmodelimage = $scope.uploadedimage2;

		else if (index == 3)
			$scope.gearsmodelimage = $scope.uploadedimage3;	
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}

})

.controller('ContactCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$translate) {

	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("מסך צור קשר");
		}
	});		
		
	$scope.contact = 
	{
		"name" : "",
		"phone" : "",
		"email" : "",
		"desc" : ""
	}

	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
			$scope.ContactImage1 = $rootScope.LanguagesJson[26][$rootScope.DefaultLanguage]; 
			//alert ($scope.ContactImage1);
	   }

    });
	
	//$scope.pagedirection = "rtl";

	
	
	  $scope.ContactError1 = $translate.instant('AddLicenseError1');
	  $scope.ContactError2 = $translate.instant('AddLicenseError2');
	  $scope.ContactError3 = $translate.instant('AddLicenseError3');
	  $scope.ContactError4 = $translate.instant('AddLicenseError4');
	  
	  $scope.ContactError5 = $translate.instant('ContactError1');	
	  $scope.ContactError6 = $translate.instant('ContactError2');	 	 	 
	  $scope.ContactError7 = $translate.instant('ContactError3');	 	 	 
	  $scope.ContactError8 = $translate.instant('ContactError4');	 	 	 
	  
	  $scope.confirmError = $translate.instant('confirm_error');

	  $rootScope.currentPage = 'contact';
	  $scope.contact.name = $localStorage.name;
	  $scope.contact.phone = $localStorage.phone;
	  $scope.contact.email = $localStorage.email;
	  
	  //alert ($rootScope.DefaultLanguage)
	  
	  if ($rootScope.DefaultLanguage == "he")
	  {
		  $scope.pagedirection = "rtl";
	  }
	  else
	  {
		  $scope.pagedirection = "ltr";
	  }


    $scope.contactStyle = {
        "direction" : $scope.pagedirection,
        "width" : "100%",
        "color" : "#fff"
    }

	
	  
$scope.sendContactBtn = function()
{
	if ($scope.contact.name =="")
	{
			$ionicPopup.alert({
			 title: $scope.ContactError5,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });			
	}
	else if ($scope.contact.phone =="" &&  $scope.contact.email =="")
	{
			$ionicPopup.alert({
			 title: $scope.ContactError6,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });			
	}
	else if ($scope.contact.desc =="")
	{
			$ionicPopup.alert({
			 title: $scope.ContactError7,
			buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
			  }]
		   });			
	}
	else
	{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				send_data = 
				{
					"user" : $localStorage.userid,
					"name" : $scope.contact.name,
					"phone" : $scope.contact.phone,
					"email" : $scope.contact.email,
					"desc" : $scope.contact.desc,
					"send" : 1			
				}
				//console.log(send_data)	
				$http.post($rootScope.Host+'/contact.php',send_data).success(function(data)
				{
				});		
				
				$ionicPopup.alert({
				 title: $scope.ContactError8,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });	
			   
		$scope.contact.name = '';
		$scope.contact.phone = '';
		$scope.contact.email = '';
		$scope.contact.desc = '';		
		$state.go('app.main');		
	}
}

})

.controller('UpdateInfoCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$translate) {


	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("מסך עידכון פרטים אישיים");
		}
	});		
		
		
	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
			$scope.UpdateInfoImage1 = $rootScope.LanguagesJson[25][$rootScope.DefaultLanguage];   
	   }

    });


	  $scope.UpdateInfoError1 = $translate.instant('UpdateInfoError1');	
	  $scope.confirmError = $translate.instant('confirm_error');


	  $scope.Gender1 = $translate.instant('register_male');	
	  $scope.Gender2 = $translate.instant('register_female');

  

	if ($rootScope.DefaultLanguage =="he")
	{
		$scope.languagesJson = $rootScope.AllHebrewCountries;
		//$scope.languagesJson = $rootScope.HebrewCountries;
	}
	else
	{
		$scope.languagesJson = $rootScope.AllEnglishCountries;
		//$scope.languagesJson = $rootScope.EnglishCountries;
	}	
	
	if ($localStorage.push_enabled == "0")
		$scope.push_enabled_field = true;
	else
		$scope.push_enabled_field = false;
		
	$scope.info = 
	{
		"name" : "",
		"phone" : "",
		"email": "",
		"birth": "",
		"country" : "",
		"gender" : "",
		"push_enabled" : $scope.push_enabled_field
	}
	
	  $scope.navTitle='<p class="title-image">עדכון פרטים אישים</p>';
	  $rootScope.currentPage = 'updateinfo';
	  $scope.info.name = $localStorage.name;
	  $scope.info.phone = $localStorage.phone;
	  $scope.info.email = $localStorage.email;
	  $scope.info.birth =  $localStorage.birthdate
	  
	  if ($localStorage.country == "")
	  {
		  $scope.info.country = '';
		  //$scope.info.country = 'IL';
	  }
	  else
	  {
		  $scope.info.country = $localStorage.country;
	  }
	  
	//  alert ($scope.info.country);
	  

	  if ($localStorage.gender == "")
	  {
		  $scope.info.gender = '';
	  }
	  else
	  {
		  $scope.info.gender = $localStorage.gender;
	  }	  
	  
	 // alert ($scope.info.gender)
	  
	
	$scope.saveInfo = function()
	{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			
			if ($scope.info.push_enabled)
				$scope.pushenabled = "0";
			else
				$scope.pushenabled = "1";
			
			
			
				send_data = 
				{
					"user" : $localStorage.userid,
					"name" : $scope.info.name,
					"phone" : $scope.info.phone,
					"email" : $scope.info.email,
					"birth" : $scope.info.birth,
					"country" : $scope.info.country,
					"gender" : $scope.info.gender,
					"push_enabled" : $scope.pushenabled,
					"send" : 1			
				}
				//console.log(send_data)	
				$http.post($rootScope.Host+'/update_user.php',send_data).success(function(data)
				{
					$localStorage.name = $scope.info.name;
					$localStorage.phone = $scope.info.phone;
					$localStorage.email = $scope.info.email;
					$localStorage.birthdate = $scope.info.birth;
					$localStorage.country = $scope.info.country;
					$localStorage.gender = $scope.info.gender;
					$localStorage.push_enabled = $scope.pushenabled;
					
				});		
				
				$ionicPopup.alert({
				 title: $scope.UpdateInfoError1,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });		

				$state.go('app.main');				   
	}
})


.controller('MainLicenseCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$cordovaCamera,$timeout,$ionicModal,$translate) {

	$scope.navTitle = 'רישיון צלילה ראשי';
	
	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("מסך תעודת צולל ראשית");
		}
	});		

	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
		 $scope.MainLicenseImage1 = $rootScope.LanguagesJson[8][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage2 = $rootScope.LanguagesJson[9][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage3 = $rootScope.LanguagesJson[10][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage4 = $rootScope.LanguagesJson[11][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage5 = $rootScope.LanguagesJson[12][$rootScope.DefaultLanguage];		
		 $scope.MainLicenseImage6 = $rootScope.LanguagesJson[13][$rootScope.DefaultLanguage];				   
	   }

    });


	
	

	 $scope.MainLicenseError1 = $translate.instant('AddLicenseError1');
	 $scope.MainLicenseError2 = $translate.instant('AddLicenseError2');
	 $scope.MainLicenseError3 = $translate.instant('AddLicenseError3');
	 $scope.MainLicenseError4 = $translate.instant('AddLicenseError4');
	 $scope.MainLicenseError5 = $translate.instant('MainLicenseError1');	 	 	 
	 $scope.confirmError = $translate.instant('confirm_error');		   
		   
	if($rootScope.licenseJson)
	{
		if ($rootScope.licenseJson[0].userlicense)
		{
			$scope.uploadedimage = $rootScope.Host+$rootScope.licenseJson[0].userlicense;
			$scope.serverimage = $rootScope.licenseJson[0].userlicense;
		}
		else
		$scope.uploadedimage = 'img/addphoto.png';
	}
	else
	{
		$scope.uploadedimage = 'img/addphoto.png';
	}
	
	if($rootScope.licenseJson)
	{
		if ($rootScope.licenseJson[0].userlicense2)
		{
			$scope.uploadedimage2 = $rootScope.Host+$rootScope.licenseJson[0].userlicense2;
			$scope.serverimage2 = $rootScope.licenseJson[0].userlicense2;
		}
		else
		$scope.uploadedimage2 = 'img/addphoto.png';
	}
	else
	$scope.uploadedimage2 = 'img/addphoto.png';
	
	$scope.CameraOption = function(imagenumber) 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: $scope.MainLicenseError1,
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: $scope.MainLicenseError2,
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1,imagenumber);
		}
	   },
	   {
		text: $scope.MainLicenseError3,
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0,imagenumber);
		}
	   },
	   	   {
		text: $scope.MainLicenseError4,
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   ]
	  });
	  

	
	 /*
	   var hideSheet = $ionicActionSheet.show({
		 buttons: [
		   { text: 'בחר תמונה מהגלרייה' },
		   { text: 'פתח מצלמה' }
		   //{ text: 'חזור'}
		 ],
		 cancelText: 'ביטול',
		 cssClass: 'social-actionsheet',
		 cancel: function() {
			  return true;
			},
		 buttonClicked: function(index) {
			 if(index<2)
			 	$scope.takePicture(index);
			 
		   return true;
		 }});
			 // For example's sake, hide the sheet after two seconds
	
	*/
	};	
	
 

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index,imagenumber) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
		/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
		*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			options.chunkedMode = false;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {
				
				if (imagenumber == 1)
				{
					$scope.image = data.response;
					$scope.uploadedimage = $rootScope.Host+data.response;
				}
				else if (imagenumber == 2)
				{
					$scope.image2 = data.response;
					$scope.uploadedimage2 = $rootScope.Host+data.response;
				}
						
					//$scope.showimage = true;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }
	
	 
	$scope.AddLicenseBtn = function()
	{
		
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			//$scope.licenseimage = ($scope.image !="" ? $scope.image : $scope.serverImage);
		
			if ($scope.image)
			{
				$scope.licenseimage = $scope.image;
			}
			else
			{
				$scope.licenseimage = $scope.serverimage;
			}
			
			if ($scope.image2)
			{
				$scope.licenseimage2 = $scope.image2;
				
			}
			else
			{
				$scope.licenseimage2 = $scope.serverimage2;
			}
				
				update_data = 
				{
					"user" : $localStorage.userid,
					"image" : $scope.licenseimage,
					"image2" : $scope.licenseimage2,
					"send" : 1
				}					
				$http.post($rootScope.Host+'/update_user_license.php',update_data).success(function(data)
				{
					
				});	

				
				$state.go('app.divinglicense');	
	}
	
	$scope.ShowImageModal = function(index)
	{
		
		if (index == 1)
		{
			$scope.licenseImageModal = $scope.uploadedimage;
		}
		else if (index == 2)
		{
			$scope.licenseImageModal = $scope.uploadedimage2;
		}
		
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
 
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}  
	
	
	$scope.deleteLicense = function()
	{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
			send_data = 
			{
				"user" : $localStorage.userid				
			}					
			$http.post($rootScope.Host+'/delete_mainlicense.php',send_data).success(function(data)
			{			
				$ionicPopup.alert({
				title: $scope.MainLicenseError5,
				buttons: [{
				text: $scope.confirmError,
				type: 'button-positive',
				}]
			   });	
				window.location = "#/app/divinglicense";
		
			});

			

	}
	
	
})

.controller('CouponsCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$cordovaCamera,$timeout,$ionicModal,$translate,ClosePopupService,$ionicScrollDelegate) {

	$scope.phpHost = $rootScope.Host;
	$scope.isClubOwner = false;
	$scope.loggedInUser = $localStorage.userid;
	
	
	$scope.$on('$ionicView.enter', function(e) {
		$scope.loggedInUser = $localStorage.userid;
		if(window.cordova)
			window.ga.trackView("מסך קופונים");
		
		$scope.getCoupons();
	});		
	
	$scope.couponfields = 
	{
		"clubselect" : "",
		"areaselect" : ""
	}

	$scope.scrollTop = function()
	{
		$ionicScrollDelegate.scrollTop(true);
	}

	$scope.getCoupons = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

	
		send_data = 
		{
			"user" : $localStorage.userid				
		}					
		$http.post($rootScope.Host+'/get_clubs_coupons.php',send_data).success(function(data)
		{			
			$scope.CouponsArray = data.coupons;
			$scope.ClubsArray = data.clubs;
			
			if (data.user.club_id > 0)
				$scope.isClubOwner = true;
			else
				$scope.isClubOwner = false;
			
			console.log("coupons: " , data);
	
		});		
	}
	
	$scope.getCoupons();
	
	

	
	$scope.deleteCoupon = function(index,id)
	{
		
		var confirmPopup = $ionicPopup.confirm({
		 title: $translate.instant('AddLicenseError7')

	   });
	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			send_data = 
			{
				"user" : $localStorage.userid,
				"id" : id,
			}					
			$http.post($rootScope.Host+'/delete_coupon.php',send_data).success(function(data)
			{			
				$scope.CouponsArray.splice(index, 1);
			});
		 } 
	   });
	}
	
	
	$scope.showDealPopup = function(index)
	{

		$scope.couponInfo = $scope.CouponsArray[index];
		
		$scope.dealpopup = $ionicPopup.show({
			templateUrl: 'templates/coupon_info_popup.html',
			scope: $scope,
			cssClass: 'infoMainDiv'
		});
		
		ClosePopupService.register($scope.dealpopup);
	}
	
	$scope.closeDealPopup = function()
	{
		$scope.dealpopup.close();
	}

	
	
	$scope.cutDate = function(date)
	{
		var dArray = date.split('-');
		dateStr = dArray[0]+"."+dArray[1]
		return dateStr;
	}
	
	$scope.cutYear = function(date)
	{
		var dArray = date.split('-');
		dateStr = dArray[2]
		return dateStr
	}	
	
})


.controller('ManageCouponCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$cordovaCamera,$timeout,$ionicModal,$translate) {

	$scope.$on('$ionicView.enter', function(e) {
		if(window.cordova){
			window.ga.trackView("עריכת/הוספת קופון");
		}
	});			

	
	
	$scope.couponfields = 
	{
		"user" : $localStorage.userid,
		"club_id" : $localStorage.club_id,
		"title" : "",
		"oldprice" : "",
		"price" : "",
		"coupondate" : "",
		"couponenddate" : "",
		"desc" : "",
		"send" : "1",
	}

	$scope.confirmError = $translate.instant('confirm_error');				
					
	$rootScope.$watch("LanguagesJson", function(){
       
	   if ($rootScope.LanguagesJson.length > 0)
	   {
			$scope.saveCouponImage = $rootScope.LanguagesJson[26][$rootScope.DefaultLanguage]; 
	   }

    });

	
	$scope.sendCoupon = function()
	{
		if ($scope.couponfields.title =="") 
		{ 
			$ionicPopup.alert({
			title: $translate.instant('inputdealtitletext'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });		
		}
		else if ($scope.couponfields.oldprice =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('inputoldpricetext'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				   
		}
		else if ($scope.couponfields.price =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('inputpricetext'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				
		}
		else if ($scope.couponfields.coupondate =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('inputcoupondatetext'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				
		}
		else if ($scope.couponfields.couponenddate =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('inputcouponenddatetext'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				
		}		
		else if ($scope.couponfields.desc =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('inputcoupondesctext'),
			buttons: [{
			text: $scope.confirmError,
			type: 'button-positive',
			}]
		   });				
		}
		else
		{

			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			$http.post($rootScope.Host+'/add_club_coupon.php',$scope.couponfields).success(function(data)
			{			
				if (data[0].status == 0)
				{
					$ionicPopup.alert({
					title: $translate.instant('couponlimittext'),
					buttons: [{
					text: $scope.confirmError,
					type: 'button-positive',
					}]
				   });						
				}
				else
				{
					$ionicPopup.alert({
					title: $translate.instant('couponsavedtext'),
					buttons: [{
					text: $scope.confirmError,
					type: 'button-positive',
					}]
				   });	
				   
					$scope.couponfields.title = '';
					$scope.couponfields.oldprice = '';
					$scope.couponfields.price = '';
					$scope.couponfields.coupondate = '';
					$scope.couponfields.couponenddate = '';
					$scope.couponfields.desc = '';
					
					$state.go('app.coupons');			
				}


		
			});

		}
		
		
	}
	
	

})
.controller('MessagesCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$cordovaCamera,$timeout,$ionicModal,$translate,ClosePopupService) {

	$scope.phpHost = $rootScope.Host;
	
	$scope.getMessages = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		license_data = 
		{
			"user" : $localStorage.userid
		}					
		$http.post($rootScope.Host+'/get_push_notifications.php',license_data).success(function(data)
		{
			$scope.pushMessages = data;
			console.log ("messages:",$scope.pushMessages)
			
		});			
	}
	
	$scope.getMessages();
	
	
	$scope.deleteMessage = function(index,id)
	{
		var confirmPopup = $ionicPopup.confirm({
		 title: $translate.instant('AddLicenseError7')

	   });
	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				delete_data = 
				{
					"user" : $localStorage.userid,
					"id" : id
				}					
				$http.post($rootScope.Host+'/delete_push_message.php',delete_data).success(function(data)
				{
					$scope.pushMessages.splice(index, 1);
				});
			
		 } 
	   });

	   
	}
	
	
	
})	

.filter('orderObjectBy', function () {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
})

.filter('showImage', function () {
    return function (value) 
	{
		if (value.status == 0)
		{
			diaryimage = 'img/n_approved.png';
		}
		else
		{
			diaryimage = 'img/approved.png';
		}
		return diaryimage;
		
    };
})

.filter('stripslashes', function () {
    return function (value) 
	{
		if(value)
		{
			return (value + '')
			.replace(/\\(.?)/g, function(s, n1) {
			  switch (n1) {
				case '\\':
				  return '\\';
				case '0':
				  return '\u0000';
				case '':
				  return '';
				default:
				  return n1;
			  }
			});
		}
		else
		{
			return ("");
		}
    };
})

.factory('ClosePopupService', function($document, $ionicPopup, $timeout){
        var lastPopup;
        return {
            register: function(popup) {
                $timeout(function(){
                    var element = $ionicPopup._popupStack.length>0 ? $ionicPopup._popupStack[0].element : null;
                    if(!element || !popup || !popup.close) return;
                    element = element && element.children ? angular.element(element.children()[0]) : null;
                    lastPopup  = popup;
                    var insideClickHandler = function(event){
                        event.stopPropagation();
                    };
                    var outsideHandler = function() {
                        popup.close();
                    };
                    element.on('click', insideClickHandler);
                    $document.on('click', outsideHandler);
                    popup.then(function(){
                        lastPopup = null;
                        element.off('click', insideClickHandler);
                        $document.off('click', outsideHandler);
                    });
                });
            },
            closeActivePopup: function(){
                if(lastPopup) {
                    $timeout(lastPopup.close);
                    return lastPopup;
                }
            }
        };
});
